//
//  Constants.swift
//  CleaningPal-Driver
//
//  Created by Bilal Saeed on 3/17/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

struct APIRoutes {
    static var baseUrl = "https://abel-backend.codesorbit.com"
    static var carImageBase = "/api/admin/carImage/"
    static var feedImageBase = "/api/admin/feedImage"
    static let login = "/api/v1/login"
    static let getCars = "/api/mobile/cars/getCars"
    static let getCarDetails = "/api/mobile/cars/getCarDetails"
    static let carValuation = "/api/mobile/cars/createCarValuation"
    static let aboutUs = "/api/mobile/about/aboutUs"
    static let carMakes = "/api/mobile/cars/getCarMakes"
    static let carModels = "/api/mobile/cars/getCarModelsByMakeId"
    static let contactedUsers = "/api/mobile/users/getContactedUsers"
    static let chatHistory = "/api/mobile/chat/getChatHistory"
    static let onlineAdmin = "/api/mobile/chat/getAdminId"
    static let getNotifications = "/api/admin/notification/get-push-notification-list"
    static let userList = "https://api.github.com/users?since="
    static let userName = "https://api.github.com/users/"
}
