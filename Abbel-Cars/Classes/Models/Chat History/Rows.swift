
import Foundation
import ObjectMapper

class Rows : Mappable {
	var id = -1
	var customerName = ""
	var customerEmail = ""
	var adminId = -1
	var message  = ""
	var sendBy = -1
	var createdAt = ""
	var updatedAt = ""

	required init?(map: Map) {

	}

    func mapping(map: Map) {

		id              <- map["id"]
		customerName    <- map["customer_name"]
		customerEmail   <- map["customer_email"]
		adminId         <- map["admin_id"]
		message         <- map["message"]
		sendBy          <- map["send_by"]
		createdAt       <- map["created_at"]
		updatedAt       <- map["updated_at"]
	}
}
