
import Foundation
import ObjectMapper

typealias ChatHistoryCompletionHandler = (_ data: ChatHistoryData?, _ error: Error?, _ status: Int?) -> Void

class ChatHistory : Mappable {
	var error = false
	var message = ""
	var data = Mapper<ChatHistoryData>().map(JSON: [:])
	var errors = [""]

	required init?(map: Map) {

	}

    func mapping(map: Map) {

		error   <- map["error"]
		message <- map["message"]
		data    <- map["data"]
		errors  <- map["errors"]
	}
    
    class func getChatHistory (_ completion: @escaping ChatHistoryCompletionHandler) {
    
        APIClient.shared.getChatHistory() { (result, error, status) in

            if error == nil {

                if let data = Mapper<ChatHistoryData>().map(JSON: result as! [String : Any]) {
                    completion(data, nil, 200)

                } else {
                    completion(nil, nil, 201)
                }

            } else {
                 completion(nil, error, 404)
            }
        }
    }
}
