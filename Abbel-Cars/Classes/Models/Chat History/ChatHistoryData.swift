
import Foundation
import ObjectMapper

class ChatHistoryData : Mappable {
	var count = -1
	var rows = [Rows]()

	required init?(map: Map) {

	}

    func mapping(map: Map) {

		count   <- map["count"]
		rows    <- map["rows"]
	}
}
