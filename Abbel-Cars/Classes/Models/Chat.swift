//
//  Chat.swift
//  Abbel-Cars
//
//  Created by a on 14/09/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import ObjectMapper

class Chat: Mappable {
    
    var adminId = -1
    var email = ""
    var message = ""
    var name = ""
    var sendBy = -1
    var type = ""
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        
        adminId     <- map["admin_id"]
        email       <- map["email"]
        message     <- map["message"]
        name        <- map["name"]
        sendBy      <- map["send_by"]
        type        <- map["type"]
    }
}
