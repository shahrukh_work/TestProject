

import Foundation
import ObjectMapper

typealias NewsFeedCompletionHandler = (_ data: NewFeedData?, _ error: Error?, _ status: Int?) -> Void

class NewsFeed : Mappable {
	var error : Bool?
	var message : String?
    var data = Mapper<NewFeedData>().map(JSON: [:])!
	var errors : [String]?

	required init?(map: Map) {

	}

    func mapping(map: Map) {

		error <- map["error"]
		message <- map["message"]
		data <- map["data"]
		errors <- map["errors"]
	}
    
    class func getNewsFeed (limit: Int,_ completion: @escaping NewsFeedCompletionHandler) {
        
        APIClient.shared.getNewsFeed(limit: limit){ (result, error, status) in
            
            if error == nil {
                
                if let data = Mapper<NewFeedData>().map(JSON: result as! [String : Any]) {
                    completion(data, nil, 200)
                    
                } else {
                    completion(nil, nil, 201)
                }
                
            } else {
                completion(nil, error, 404)
            }
        }
    }

}
