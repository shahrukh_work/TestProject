//
//  NewsFeedItem.swift
//  Abbel-Cars
//
//  Created by Mac on 15/09/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import ObjectMapper

class NewsFeedItem : Mappable {
    var title = ""
    var image = ""
    var message = ""

    required init?(map: Map) {}

    func mapping(map: Map) {
        title <- map["title"]
        image <- map["image"]
        message <- map["message"]
    }

}
