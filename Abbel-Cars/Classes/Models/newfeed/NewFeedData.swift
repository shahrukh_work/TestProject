

import Foundation
import ObjectMapper

class NewFeedData : Mappable {
    var data = [NewsFeedItem]()
    var count = -1
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        data <- map["data"]
        count <- map["count"]
    }
}
