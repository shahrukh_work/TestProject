

import Foundation
import ObjectMapper

typealias AboutUsCompletionHandler = (_ data: AboutData?, _ error: Error?, _ status: Int?) -> Void

class About : Mappable {
	var error = false
	var message = ""
    var data = Mapper<AboutData>().map(JSON: [:])
    var errors = [""]

	required init?(map: Map) {
	}

    func mapping(map: Map) {

		error <- map["error"]
		message <- map["message"]
		data <- map["data"]
		errors <- map["errors"]
	}

    class func getAboutUs (_ completion: @escaping AboutUsCompletionHandler) {
    
        APIClient.shared.getAboutUs() { (result, error, status) in
            
            if error == nil {
                
                if let data = Mapper<AboutData>().map(JSON: result as! [String : Any]) {
                    //print("jllo")
                    completion(data, nil, 200)
                    
                } else {
                    completion(nil, nil, 201)
                }
                
            } else {
                 completion(nil, error, 404)
            }
        }
    }
}
