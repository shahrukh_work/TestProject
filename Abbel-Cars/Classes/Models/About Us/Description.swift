
import Foundation
import ObjectMapper

class Description : Mappable {
	var about = ""
    var id = -1
    var image = ""
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        about <- map["about"]
        image <- map["image"]
    }
}
