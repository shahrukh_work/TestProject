
import Foundation
import ObjectMapper

class AboutData : Mappable {
    var description = [Description]()

	required init?(map: Map) {

	}

    func mapping(map: Map) {

		description <- map["data"]
	}
}
