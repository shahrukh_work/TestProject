
import Foundation
import ObjectMapper

typealias CarModelCompletionHandler = (_ data: CarModelData?, _ error: Error?, _ status: Int?) -> Void

class CarModels : Mappable {
	var error = false
	var message = ""
    var carModel = Mapper<CarModelData>().map(JSON: [:])
	var errors = [""]

	required init?(map: Map) {
	}

    func mapping(map: Map) {

		error       <- map["error"]
		message     <- map["message"]
		carModel    <- map["data"]
		errors      <- map["errors"]
	}
    
    class func getCarModels (id: Int, _ completion: @escaping CarModelCompletionHandler) {
        
        APIClient.shared.getCarModels(id: id) { (result, error, status) in
            
            if error == nil {
                
                if let data = Mapper<CarModelData>().map(JSON: result as! [String : Any]) {
                    completion(data, nil, 200)
                    
                } else {
                    completion(nil, nil, 201)
                }
                
            } else {
                 completion(nil, error, 404)
            }
        }
    }
}
