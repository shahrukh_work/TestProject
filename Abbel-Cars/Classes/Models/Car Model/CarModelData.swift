
import Foundation
import ObjectMapper

class CarModelData : Mappable {
	var data  = [CarModel]()
	var count = -1

	required init?(map: Map) {
	}

    func mapping(map: Map) {

		data    <- map["data"]
		count   <- map["count"]
	}
}
