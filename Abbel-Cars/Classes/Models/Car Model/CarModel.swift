
import Foundation
import ObjectMapper

class CarModel : Mappable {
	var id = -1
	var name = ""
	var carMakeId = -1
	var createdAt = ""
	var updatedAt = ""

	required init?(map: Map) {
	}

    func mapping(map: Map) {

		id          <- map["id"]
		name        <- map["name"]
		carMakeId   <- map["car_make_id"]
		createdAt   <- map["created_at"]
		updatedAt   <- map["updated_at"]
	}
}
