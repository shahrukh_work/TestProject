
import Foundation
import ObjectMapper

typealias CarValuationCompletionHandler = (_ data: Any?, _ error: Error?, _ status: Int?) -> Void

class CarValuation : Mappable {
	var error = false
	var message = ""
	var data = ""
	var errors = [""]

	required init?(map: Map) {}

	func mapping(map: Map) {

		error   <- map["error"]
		message <- map["message"]
		data    <- map["data"]
		errors  <- map["errors"]
	}
    
    class func createCarValuation(name: String, email: String, phoneNo: String, regNo: String,
                                  carMake: String, carModel: String, engineCapacity: String,
                                  color: String, klms: Int, carBadge: String, fuelType: String,
                                  bodyType: String, transmission: String, makeYear: Int,
                                  _ completion: @escaping CarValuationCompletionHandler) {
        
        APIClient.shared.createCarValuation(name: name, email: email, phoneNo: phoneNo, regNo: regNo, carMake: carMake, carModel: carModel, engineCapacity: engineCapacity, color: color, klms: klms, carBadge: carBadge, fuelType: fuelType, bodyType: bodyType, transmission: transmission, makeYear: makeYear) { (result, error, status) in
            
            if error == nil {
                
                if let data =  result as? Any {
                    completion(data, nil, 200)
                    
                } else {
                    completion(nil, nil, 201)
                }
            
            } else {
                completion(nil, error, 404)
            }
        }
    }
}
