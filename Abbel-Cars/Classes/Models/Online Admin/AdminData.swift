
import Foundation
import ObjectMapper

class AdminData : Mappable {
	var id = -1

	required init?(map: Map) {

	}

    func mapping(map: Map) {

		id <- map["id"]
	}
}
