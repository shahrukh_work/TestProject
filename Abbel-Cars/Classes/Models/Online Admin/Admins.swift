
import Foundation
import ObjectMapper

typealias GetAdminCompletionHandler = (_ data: AdminData?, _ error: Error?, _ status: Int?) -> Void

class Admins : Mappable {
	var error = false
	var message = ""
	var data = Mapper<AdminData>().map(JSON: [:])
	var errors = [""]

	required init?(map: Map) {

	}

    func mapping(map: Map) {

		error   <- map["error"]
		message <- map["message"]
		data    <- map["data"]
		errors  <- map["errors"]
	}
    
    class func getOnlineAdmin (completion: @escaping GetAdminCompletionHandler) {
        
        APIClient.shared.getOnlineAdminMethod { (result, error, status) in

            if error == nil {

                if let data = Mapper<AdminData>().map(JSON: result as! [String : Any]) {
                    completion(data,nil,status)
                    return
                }
                completion(nil,error,status)

            } else {
                completion(nil,error,status)
            }
        }
    }
    
}
