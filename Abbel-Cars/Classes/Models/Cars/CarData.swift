
import Foundation
import ObjectMapper

class CarData : Mappable {
    var cars = [Car]()
	var count = -1

    required init?(map: Map) {
        
    }

    func mapping(map: Map) {
		cars  <- map["data"]
		count <- map["count"]
	}
}
