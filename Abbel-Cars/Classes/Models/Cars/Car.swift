
import Foundation
import ObjectMapper

class Car : Mappable {
	var id = -1
	var regNo = ""
	var image = ""
	var badge = ""
	var make = ""
	var model = ""
	var price = -1
	var color = ""
	var klms = -1
	var year = -1
	var fuelType = ""
	var bodyType = ""
	var transmission = ""
	var engineCapacity = ""
	var description = ""
	var adSource = ""
	var status = -1
	var createdBy = -1
	var createdAt = ""
	var updatedAt = ""
    var isFavorite = false //non-mapping

	required init?(map: Map) {
	}

    func mapping(map: Map) {

		id                 <- map["id"]
		regNo              <- map["reg_no"]
		image              <- map["image"]
		badge              <- map["badge"]
		make               <- map["make"]
		model              <- map["model"]
		price              <- map["price"]
		color              <- map["color"]
		klms               <- map["klms"]
		year               <- map["year"]
		fuelType           <- map["fuel_type"]
		bodyType           <- map["body_type"]
		transmission       <- map["transmission"]
		engineCapacity     <- map["engine_capacity"]
		description        <- map["description"]
		adSource           <- map["ad_source"]
		status             <- map["status"]
		createdBy          <- map["created_by"]
		createdAt          <- map["created_at"]
		updatedAt          <- map["updated_at"]
	}
}
