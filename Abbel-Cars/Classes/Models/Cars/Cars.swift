
import Foundation
import ObjectMapper

typealias GetCarsCompletionHandler = (_ data: CarData?, _ error: Error?, _ status: Int?) -> Void

class Cars : Mappable {
	var error = false
	var message = ""
    var data = Mapper<CarData>().map(JSON: [:])
	var errors = [""]

    required init?(map: Map) {

	}

    func mapping(map: Map) {

		error       <- map["error"]
		message     <- map["message"]
		data        <- map["data"]
		errors      <- map["errors"]
	}
    
    class func getCars (offset: Int, limit: Int, regNo: String,
                        makeYear: Int, carMake: String, carModel: String,
                        engineCapacity: String, color: String,
                        fuelType: String, carBadge: String,
                        transmission: String, minKlms: Int, maxKlms: Int,
                        maxPrice: Int, minPrice: Int, orderColumn: String, orderType: String,
                        _ completion: @escaping GetCarsCompletionHandler) {
    
        APIClient.shared.getAllCars(offset: offset, limit: limit, regNo: regNo, makeYear: makeYear, carMake: carMake, carModel: carModel, engineCapacity: engineCapacity, color: color, fuelType: fuelType, carBadge: carBadge, transmission: transmission, minKlms: minKlms, maxKlms: maxKlms, maxPrice: maxPrice, minPrice: minPrice,orderColumn: orderColumn, orderType: orderType ) { (result, error, status) in
            
            if error == nil {
        
                if let data = Mapper<CarData>().map(JSON: result as! [String : Any]) {
                    completion(data, nil, 200)
                    
                } else {
                    completion(nil, nil, 201)
                }
                
            } else {
                 completion(nil, error, 404)
            }
        }
    }
    
    class func getCarsBySearch (offset: Int, limit: Int, searchKey: String,
                        _ completion: @escaping GetCarsCompletionHandler) {
    
        APIClient.shared.getAllCarsBySearch(offset: offset, limit: limit, searchKey: searchKey ) { (result, error, status) in
            
            if error == nil {
        
                if let data = Mapper<CarData>().map(JSON: result as! [String : Any]) {
                    completion(data, nil, 200)
                    
                } else {
                    completion(nil, nil, 201)
                }
                
            } else {
                 completion(nil, error, 404)
            }
        }
    }
}
