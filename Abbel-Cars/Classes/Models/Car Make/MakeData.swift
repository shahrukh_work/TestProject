
import Foundation
import ObjectMapper

class MakeData : Mappable {
	var make = [Make]()
	var count = -1

	required init?(map: Map) {
	}

    func mapping(map: Map) {

		make    <- map["data"]
		count   <- map["count"]
	}
}
