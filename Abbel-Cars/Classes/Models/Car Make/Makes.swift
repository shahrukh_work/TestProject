
import Foundation
import ObjectMapper

typealias CarMakeCompletionHandler = (_ data: MakeData?,_ error: Error?,_ status: Int?) -> Void

class Makes : Mappable {
	var error = false
	var message = ""
	var data = Mapper<MakeData>().map(JSON: [:])
	var errors = [""]

	required init?(map: Map) {
	}

    func mapping(map: Map) {

		error   <- map["error"]
		message <- map["message"]
		data    <- map["data"]
		errors  <- map["errors"]
	}
    
    class func getCarMakes(_ completion: @escaping CarMakeCompletionHandler) {
        
        APIClient.shared.getCarMakes() { (result, error, status) in
            
            if error == nil {
                
                if let data = Mapper<MakeData>().map(JSON: result as! [String : Any]) {
                    completion(data, nil, 200)
                    
                } else {
                    completion(nil, nil, 201)
                }
                
            } else {
                 completion(nil, error, 404)
            }
        }
    }
}
