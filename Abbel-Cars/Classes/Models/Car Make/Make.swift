
import Foundation
import ObjectMapper

class Make : Mappable {
	var id = -1
	var name = ""
	var createdAt = ""
	var updatedAt = ""

	required init?(map: Map) {
	}

    func mapping(map: Map) {

		id          <- map["id"]
		name        <- map["name"]
		createdAt   <- map["created_at"]
		updatedAt   <- map["updated_at"]
	}
}
