//
//  StartJob.swift
//  CleaningPal-Driver
//
//  Created by Mac on 10/07/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import ObjectMapper

typealias StartJobCompletionHandler = (_ data: String?, _ error: Error?,_ message: Int?) -> ()

class StartJob : Mappable {
    var error : Bool = false
    var message : String = ""
    var data = ""
    var errors : [String] = []
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        error <- map["error"]
        message <- map["message"]
        data <- map["data"]
        errors <- map["errors"]
    }
    
    class func setStartJob (jobId: Int, completion: @escaping StartJobCompletionHandler) {
        
        Utility.showLoading()
        APIClient.shared.setStartJobMethod(jobId: jobId) { (result, error, status) in
            Utility.hideLoading()
            
            if error == nil {
                completion("Job Started!",nil,status)
                
            } else {
                completion(nil,error,status)
            }
        }
    }
}
