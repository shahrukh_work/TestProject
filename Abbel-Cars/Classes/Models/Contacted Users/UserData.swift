
import Foundation
import ObjectMapper

class UserData : Mappable {
	var user = [User]()
	var count = -1

	required init?(map: Map) {}

    func mapping(map: Map) {

		user    <- map["data"]
		count   <- map["count"]
	}
}
