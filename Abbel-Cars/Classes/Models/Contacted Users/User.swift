
import Foundation
import ObjectMapper

class User : Mappable {
	var id = -1
	var name = ""
	var phone = ""
	var startDatetime = ""
	var endDatetime = ""
	var status = -1
	var createdAt = ""
	var updatedAt = ""

	required init?(map: Map) {}

    func mapping(map: Map) {

		id              <- map["id"]
		name            <- map["name"]
		phone           <- map["phone"]
		startDatetime  <- map["start_datetime"]
		endDatetime    <- map["end_datetime"]
		status          <- map["status"]
		createdAt      <- map["created_at"]
		updatedAt      <- map["updated_at"]
	}
}
