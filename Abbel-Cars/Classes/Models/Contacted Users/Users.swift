
import Foundation
import ObjectMapper

typealias GetUsersCompletionHandler = (_ data: UserData?, _ error: Error?, _ status: Int?) -> Void

class Users : Mappable {
	var error = false
	var message = ""
    var data = Mapper<UserData>().map(JSON: [:])
	var errors = [""]

	required init?(map: Map) {}

    func mapping(map: Map) {

		error   <- map["error"]
		message <- map["message"]
		data    <- map["data"]
		errors  <- map["errors"]
	}
    
    class func getContactedUsers(offset: Int, limit: Int, _ completion: @escaping GetUsersCompletionHandler) {
    
        APIClient.shared.getContactedUsers(offset: offset, limit: limit) { (result, error, status) in
            
            if error == nil {
        
                if let data = Mapper<UserData>().map(JSON: result as! [String : Any]) {
                    completion(data, nil, 200)
                    
                } else {
                    completion(nil, nil, 201)
                }
                
            } else {
                 completion(nil, error, 404)
            }
        }
    }
}
