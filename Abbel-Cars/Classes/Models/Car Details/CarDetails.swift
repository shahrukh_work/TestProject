
import Foundation
import ObjectMapper

typealias CarDetailsCompletionHandler = (_ data: CarDetailsData?, _ error: Error?, _ status: Int?) -> Void

class CarDetails : Mappable {
	var error = false
	var message = ""
	var data =  Mapper<CarDetailsData>().map(JSON: [:])
	var errors = [""]

	required init?(map: Map) {
	}

    func mapping(map: Map) {

		error   <- map["error"]
		message <- map["message"]
		data    <- map["data"]
		errors  <- map["errors"]
	}
    
    class func getCarDetails (id: Int,_ completion: @escaping CarDetailsCompletionHandler) {
    
        APIClient.shared.getCarDetails(id: id) { (result, error, status) in
            
            if error == nil {
                
                if let data = Mapper<CarDetailsData>().map(JSON: result as! [String : Any]) {
                    //print("jllo")
                    completion(data, nil, 200)
                    
                } else {
                    completion(nil, nil, 201)
                }
                
            } else {
                 completion(nil, error, 404)
            }
        }
    }
}
