
import Foundation
import ObjectMapper

class CarDetailsData : Mappable {
	var carDetail = [CarDetail]()

	required init?(map: Map) {
	}

    func mapping(map: Map) {

		carDetail <- map["data"]
	}
}
