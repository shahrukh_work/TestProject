//
//  DataManager.swift
//  ShaeFoodDairy
//
//  Created by Mac on 01/04/2020.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
import ObjectMapper

class DataManager {
    
    static let shared = DataManager()
    
    func setStringData (value: String, key: String) {
        UserDefaults.standard.set(value, forKey: key)
    }
    
    func setIntData (value: Int, key: String) {
        UserDefaults.standard.set(value, forKey: key)
    }
    
//    func getUser() -> User? {
//        var user: User?
//
//        if UserDefaults.standard.object(forKey: "user_data") != nil {
//            user = Mapper<User>().map(JSONString:UserDefaults.standard.string(forKey: "user_data")!)
//        }
//        return user
//    }
    
    func deleteUser () {
         UserDefaults.standard.set(nil, forKey: "user_data")
    }
    
    func setAuthentication (auth: String) {
        UserDefaults.standard.set(auth, forKey: "auth_data")
    }
    
//    func getAuthentication() -> AuthLogin? {
//        var auth: AuthLogin?
//        
//        if UserDefaults.standard.object(forKey: "auth_data") != nil {
//            auth = Mapper<AuthLogin>().map(JSONString:UserDefaults.standard.string(forKey: "auth_data")!)
//        }
//        return auth
//    }
    
    func deleteAuthentication () {
        UserDefaults.standard.set(nil, forKey: "auth_data")
    }
    
    
    func setFavoriteId (id: Int) {
        var newItem: [Int] = []
        var filterData: [Int] = []
        //getFavoriteIds()
        if getFavoriteIds() != nil && getFavoriteIds()!.count != 0 {
            newItem = getFavoriteIds()!
            filterData = filterData.filter({$0 == id})
        }
        
        if filterData.count == 0 {
            newItem.append(id)
        }
        UserDefaults.standard.set(newItem, forKey: "fav_data")
    }
    
    func getFavoriteIds() -> [Int]? {
        var items: [Int]?
        
        if UserDefaults.standard.array(forKey: "fav_data") != nil {
            items = UserDefaults.standard.array(forKey: "fav_data") as? [Int]
        }
        return items
    }
    
    func removeFavoriteId(id: Int) {
        let newItem = getFavoriteIds()
        let items = newItem?.filter({$0 != id})
        //items?.removeFirst()
        UserDefaults.standard.set(items, forKey: "fav_data")
    }
    
    func removeFilters() {
        UserDefaults.standard.set(nil, forKey: "RegNo")
        UserDefaults.standard.set(nil, forKey: "carMake")
        UserDefaults.standard.set(nil, forKey: "carModel")
        UserDefaults.standard.set(nil, forKey: "engineCapacity")
        UserDefaults.standard.set(nil, forKey: "color")
        UserDefaults.standard.set(nil, forKey: "fuelType")
        UserDefaults.standard.set(nil, forKey: "carBadge")
        UserDefaults.standard.set(nil, forKey: "minKM")
        UserDefaults.standard.set(nil, forKey: "maxKM")
        UserDefaults.standard.set(nil, forKey: "transmission")
        UserDefaults.standard.set(nil, forKey:"bodyType")
        UserDefaults.standard.set(nil, forKey: "minPrice")
        UserDefaults.standard.set(nil, forKey: "maxPrice")
        UserDefaults.standard.set(nil, forKey: "makeYear")
        UserDefaults.standard.set(nil, forKey: "sortBy")
    }
}
