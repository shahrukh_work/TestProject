//
//  Utility.swift
//  Audtix
//
//  Created by Bilal Saeed on 9/14/19.
//  Copyright © 2019 Bilal Saeed. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Alamofire
import SocketIO


struct NetworkingConnection {
  static let sharedInstance = NetworkReachabilityManager()!
  static var isConnectedToInternet:Bool {
      return self.sharedInstance.isReachable
    }
}

@objc class Utility: NSObject {
    
    class func getAppDelegate() -> AppDelegate? {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        return appDelegate
    }
    
    class func changeDateFormate (dataInString : String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ" //2020-09-18T07:23:28.000Z
        let localDate = dateFormatter.date(from: dataInString)
        
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .short
        dateFormatter.dateFormat = "dd MMM yyyy, HH:mm"
        if localDate != nil {
            return dateFormatter.string(from: localDate!)
            
        } else {
            return ""
        }
    }
}
