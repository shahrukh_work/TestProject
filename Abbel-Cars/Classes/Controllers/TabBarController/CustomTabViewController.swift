//
//  CustomTabViewController.swift
//  STTabbar_Example
//
//  Created by Shraddha Sojitra on 19/06/20.
//  Copyright © 2020 CocoaPods. All rights reserved.
//

import UIKit
import STTabbar

class CustomTabViewController: UITabBarController {
    
    //MARK:- Variables
    var homeNavController = UINavigationController()
    var chatnavController = UINavigationController()
    
    
    //MARK:- Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        if let myTabbar = tabBar as? STTabbar {
            myTabbar.centerButtonActionHandler = {
            
//                let valuationVC = ValuationViewController(nibName: "ValuationViewController", bundle: nil)
//                valuationVC.modalPresentationStyle = .overFullScreen
//                self.present(valuationVC, animated: true, completion: nil)
            }
        }
        
        let selectedColor   = #colorLiteral(red: 0.7882352941, green: 0.1568627451, blue: 0.1764705882, alpha: 1)
        let unselectedColor = #colorLiteral(red: 0.5960116386, green: 0.59611696, blue: 0.5960050225, alpha: 1)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: unselectedColor], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: selectedColor], for: .selected)
        
//        let homeVC = CarsViewController()
//        homeVC.tabBarItem.image = UIImage(named: "Group 1595")!.withRenderingMode(.alwaysOriginal)
//        homeVC.tabBarItem.selectedImage = UIImage(named: "Cars")!.withRenderingMode(.alwaysOriginal)
//        homeVC.tabBarItem.imageInsets = UIEdgeInsets(top: 5, left: 0, bottom: -5, right: 0);  // sets UItabBarItem position according to given constants
//        homeNavController = UINavigationController(rootViewController: homeVC)
        
        let askQuestionVC = AskQuestionViewController()
        askQuestionVC.tabBarItem.image = UIImage(named: "Chat")!.withRenderingMode(.alwaysOriginal)
        askQuestionVC.tabBarItem.selectedImage = UIImage(named: "Group 1593")!.withRenderingMode(.alwaysOriginal)
        askQuestionVC.tabBarItem.imageInsets = UIEdgeInsets(top: 5, left: 20, bottom: -5, right: -20);    // sets UItabBarItem position according to given constants
        chatnavController = UINavigationController(rootViewController: askQuestionVC)
  
    }
}
