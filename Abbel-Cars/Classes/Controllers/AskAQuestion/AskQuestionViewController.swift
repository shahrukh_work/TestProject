//
//  AskQuestionViewController.swift
//  Abbel-Cars
//
//  Created by a on 23/08/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit

class AskQuestionViewController: UIViewController {

    //MARK:- IBOutlets
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var fullNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var sendButton: UIButton!
    
    
    //MARK:- Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    
    //MARK:- View Setup
    func setupView() {
        navigationController?.setNavigationBarHidden(true, animated: false)
        fullNameTextField.delegate = self
        emailTextField.delegate = self
        fullNameTextField.setLeftPaddingPoints(5)
        emailTextField.setLeftPaddingPoints(5)
        
        fullNameTextField.clipsToBounds = true
        fullNameTextField.layer.cornerRadius = 2
        fullNameTextField.layer.masksToBounds = true
        fullNameTextField.layer.borderColor = #colorLiteral(red: 0.4392156863, green: 0.4392156863, blue: 0.4392156863, alpha: 1)
        fullNameTextField.layer.borderWidth = 1
        
        emailTextField.clipsToBounds = true
        emailTextField.layer.cornerRadius = 2
        emailTextField.layer.masksToBounds = true
        emailTextField.layer.borderColor = #colorLiteral(red: 0.4392156863, green: 0.4392156863, blue: 0.4392156863, alpha: 1)
        emailTextField.layer.borderWidth = 1
        
        cardView.layer.masksToBounds = false
        cardView.layer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0.1607843137, alpha: 1)
        cardView.layer.shadowOpacity = 0.3
        cardView.layer.shadowOffset = .zero
        cardView.layer.shadowRadius = 5
        
        sendButton.applyGradient(colors: [#colorLiteral(red: 0.8705882353, green: 0.2980392157, blue: 0.3725490196, alpha: 1),#colorLiteral(red: 0.862745098, green: 0.1529411765, blue: 0.2509803922, alpha: 1)])
        sendButton.clipsToBounds = true
        sendButton.layer.cornerRadius = 5
        
        if UserDefaults.standard.string(forKey: "email") != nil && UserDefaults.standard.string(forKey: "fullName") != nil {
            fullNameTextField.text = UserDefaults.standard.string(forKey: "fullName")
            emailTextField.text = UserDefaults.standard.string(forKey: "email")
        }
        
    }
    
    
    //MARK:- IBActions
    @IBAction func closeButtonTapped(_ sender: Any) {
        self.tabBarController?.selectedIndex = 0
    }
    
    @IBAction func SendButtonTapped(_ sender: UIButton) {
        if fullNameTextField.text!.isEmpty {
            self.showAlert(title: "Error", message: "Please enter your name")
        }
        else if emailTextField.text!.isEmpty {
            self.showAlert(title: "Error", message: "Please enter your email address")
        }
        else if !emailTextField.isValidEmail(emailTextField.text!) {
            self.showAlert(title: "Error", message: "Invalid Email Address")
        }
        else {
            UserDefaults.standard.set(fullNameTextField.text!, forKey: "fullName")
            UserDefaults.standard.set(emailTextField.text!, forKey: "email")
            UserDefaults.standard.set(true, forKey: "isUserRegistered")
         
        }
    }
}


//MARK:- Textfield Delegates
extension AskQuestionViewController : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 50
        let currentString: NSString = (textField.text ?? "") as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
}
