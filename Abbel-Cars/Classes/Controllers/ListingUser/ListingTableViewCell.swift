//
//  ListingTableViewCell.swift
//  Abbel-Cars
//
//  Created by Macbook Pro on 02/03/2021.
//  Copyright © 2021 Mian Faizan Nasir. All rights reserved.
//

import UIKit
import SDWebImage

class ListingTableViewCell: UITableViewCell {

    @IBOutlet weak var noteButton: UIButton!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var detail: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    var cellImage: UIImage = #imageLiteral(resourceName: "10")
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func config(user: UserListing, indexPath: IndexPath) {
        nameLabel.text = user.login
        
        let isNote = CoreDataHandler().containsNote(userName: user.login ?? "")
        
        
        if isNote {
            noteButton.isHidden = false
        } else {
            noteButton.isHidden = true
        }
        
        userImageView.downloaded(from: URL(string: user.avatar_url!)!)
        
        DispatchQueue.global().async {
            let data = try? Data(contentsOf: URL(string: user.avatar_url!)!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
            
            DispatchQueue.main.async {
                
                if data != nil {
                    self.cellImage = UIImage(data: data!)!
                }
                
                if indexPath.row % 4 == 0 {
                    
                    let beginImage = CIImage(image: self.cellImage ?? #imageLiteral(resourceName: "10"))
                    
                    if let filter = CIFilter(name: "CIColorInvert") {
                        filter.setValue(beginImage, forKey: kCIInputImageKey)
                        let newImage = UIImage(ciImage: filter.outputImage!)
                        self.userImageView!.image = newImage
                    }
                    
                } else {
                    self.userImageView!.image = self.cellImage
                }
            }
        }
    }
}
