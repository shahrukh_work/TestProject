//
//  GitUserViewController.swift
//  Abbel-Cars
//
//  Created by Macbook Pro on 03/03/2021.
//  Copyright © 2021 Mian Faizan Nasir. All rights reserved.
//

import UIKit
import ObjectMapper
import SDWebImage
import CoreImage

class GitUserViewController: UIViewController {

    
    @IBOutlet weak var noteTextview: UITextView!
    @IBOutlet weak var blogLabel: UILabel!
    @IBOutlet weak var companyLabel: UILabel!
    @IBOutlet weak var followingLabel: UILabel!
    @IBOutlet weak var profileImageview: UIImageView!
    @IBOutlet weak var followerLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    
    var userName: String?
    var userData = Mapper<GitUser>().map(JSON: [:])
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getUser(userName: userName ?? "")
    }
    
    
    func getUser (userName: String) {
        
        GitUser.getGitUser(username: userName) { [self] (result, error, status) in
            
            let coreDataNote = CoreDataHandler().getNote(userName: userName)
            noteTextview.text = coreDataNote?.note
            if status == 201 {
               
                if !CoreDataHandler().gitUserDetails().isEmpty {
                    self.userData = CoreDataHandler().gitUserDetails().first
                    
                    if userName == self.userData?.login {
                        self.followerLabel.text = "\(self.userData?.followers ?? 0)"
                        self.followingLabel.text = "\(self.userData?.following ?? 0)"
                        self.nameLabel.text = self.userData?.avatar_url ?? ""
                        self.companyLabel.text = self.userData?.company ?? ""
                        self.blogLabel.text = self.userData?.blog ?? ""
                        self.profileImageview.image = UIImage(data: self.userData?.data ?? Data())
                        DispatchQueue.global().async {
                            let data = self.userData?.data 
                            DispatchQueue.main.async {
                                profileImageview.image = UIImage(data: data!)
                            }
                        }
                        return
                    }
                }
                return
            }
            
            if error == nil {
                self.userData = result
                self.followerLabel.text = "\(result?.followers ?? 0)"
                self.followingLabel.text = "\(result?.following ?? 0)"
                self.nameLabel.text = result?.avatar_url ?? ""
                self.companyLabel.text = result?.company ?? ""
                self.blogLabel.text = result?.blog ?? ""
                DispatchQueue.global().async {
                    let data = try? Data(contentsOf: URL(string: (result?.avatar_url ?? "")!)!)
                    
                    DispatchQueue.main.async {
                        profileImageview.image = UIImage(data: data!)
                        CoreDataHandler().saveGitUserDetail(userName: userName, followers: Int16(result?.followers ?? 0), following: Int16(result?.following ?? 0), avatar_url: result?.avatar_url ?? "", company: result?.company ?? "", blog: result?.blog ?? "", image: data!)
                    }
                }
            }
            
            
        }
    }
    
    
    @IBAction func savePressed(_ sender: Any) {
        CoreDataHandler().saveNote(userName: userName ?? "", note: noteTextview.text)
    }
}
