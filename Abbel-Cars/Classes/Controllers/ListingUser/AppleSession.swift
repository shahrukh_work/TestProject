//
//  AppleSession.swift
//  Abbel-Cars
//
//  Created by Macbook Pro on 03/03/2021.
//  Copyright © 2021 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import Alamofire


typealias URLSessionCompletionHandler = (_ result: AnyObject?, _ error: NSError?,_ statusCode: Int) -> Void

class AppleSession {
    
    
    func sendRequest (urlString: String, _ completion: @escaping URLSessionCompletionHandler) {
        let url = URL(string: urlString)!
        if !NetworkReachabilityManager()!.isReachable {
            completion(nil , nil, 201)
            return
        }
        let task = URLSession.shared.dataTask(with: url) {(result, response, error) in
            guard let data = result else { return }
            do {
                let jsonObject = try JSONSerialization.jsonObject(with: data)
                if let dictionary = jsonObject as? AnyObject {
                    DispatchQueue.main.async {
                        completion(dictionary , error as NSError?, 200)
                        
                    }
                }
                
            } catch {
                
            }
        }
        task.resume()
    }
}
