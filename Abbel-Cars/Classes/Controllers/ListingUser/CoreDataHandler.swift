//
//  CoreDataHandler.swift
//  Abbel-Cars
//
//  Created by Macbook Pro on 04/03/2021.
//  Copyright © 2021 Mian Faizan Nasir. All rights reserved.
//

import Foundation
import CoreData
import ObjectMapper

public class CoreDataHandler
{
    var context:NSManagedObjectContext
    
    init() {
        context = CoreDataManager.sharedInstance.managedObjectContext
    }

    //MARK: - save & delete video path and file
    func saveVideoPath (login: String = "" , id : Int16 = 0, node_id: String = "", avatar_url: String = "" ,gravatar_id: String = "", html_url: String = "", followers_url: String = "", following_url: String = "", type: String = "", site_admin: Bool = false) {
       // deleteRecordList()
        let gitUsersData = GitUsersData(context: context)
        gitUsersData.login = login
        gitUsersData.id = id
        gitUsersData.node_id = node_id
        gitUsersData.avatar_url = avatar_url
        gitUsersData.gravatar_id = gravatar_id
        gitUsersData.html_url = html_url
        gitUsersData.followers_url = followers_url
        gitUsersData.following_url = following_url
        gitUsersData.type = type
        gitUsersData.site_admin = site_admin
        CoreDataManager.sharedInstance.saveContext()
    }
    
    func saveGitUserDetail (userName: String, followers: Int16 = 0, following : Int16 = 0, avatar_url: String = "", company: String = "", blog: String = "", image: Data) {

        //deleteRecord()
        let gitUsersData = GitUserDetail(context: context)
        gitUsersData.followers = followers
        gitUsersData.following = following
        gitUsersData.avatar_url = avatar_url
        gitUsersData.company = company
        gitUsersData.blog = blog
        gitUsersData.imageUser = image
        gitUsersData.userName = userName
        CoreDataManager.sharedInstance.saveContext()
    }
    
    func saveNote (userName: String, note: String = "") {
        let noteData = NoteData(context: context)
        noteData.userName = userName
        noteData.note = note
        CoreDataManager.sharedInstance.saveContext()
    }
    
    func getNote (userName: String) -> NoteData? {
        var data: NoteData?
        do{
            let fetchRequest: NSFetchRequest<NoteData> = NoteData.fetchRequest()
            fetchRequest.predicate = NSPredicate(format: "userName == %@", userName)
            let data = try context.fetch(fetchRequest)
            return data.first

        } catch {
            
        }
        return nil
    }
    
    func containsNote (userName: String) -> Bool {
        var data: NoteData?
        do{
            let fetchRequest: NSFetchRequest<NoteData> = NoteData.fetchRequest()
            fetchRequest.predicate = NSPredicate(format: "userName == %@", userName)
            let data = try context.fetch(fetchRequest)
            
            if data.count > 0 {
                return true
            }
            return false

        } catch {
            
        }
        return false
    }
    
    func getSaveVideoPath () -> [UserListing] {
        var data: GitUsersData?
        var result: [UserListing] = []
        do{
            let fetchRequest: NSFetchRequest<GitUsersData> = GitUsersData.fetchRequest()
            let data = try context.fetch(fetchRequest)
            
            for item in data {
                let userList =  Mapper<UserListing>().map(JSON: [:])!
                userList.login = item.login
                userList.id = Int(item.id)
                userList.node_id = item.node_id
                userList.avatar_url = item.avatar_url
                userList.gravatar_id = item.gravatar_id
                userList.html_url = item.html_url
                userList.followers_url = item.followers_url
                userList.following_url = item.following_url
                userList.type = item.type
                userList.site_admin = item.site_admin
                result.append(userList)
            }
            return result

        } catch {
            
        }
        return []
    }
    
    
    func deleteRecord () {

        do{
            let fetchRequest: NSFetchRequest<GitUserDetail> = GitUserDetail.fetchRequest()
            let data = try context.fetch(fetchRequest)
            
            for item in data {
                context.delete(item)
            }

        } catch {
            
        }
    }
    
    func deleteRecordList () {

        do{
            let fetchRequest: NSFetchRequest<GitUsersData> = GitUsersData.fetchRequest()
            let data = try context.fetch(fetchRequest)
            
            for item in data {
                context.delete(item)
            }

        } catch {
            
        }
    }
    
    
    func gitUserDetails () -> [GitUser] {
        var dataResult: [GitUser] = []
        var result: [GitUserDetail] = []
        do{
            let fetchRequest: NSFetchRequest<GitUserDetail> = GitUserDetail.fetchRequest()
            var data = try context.fetch(fetchRequest)
            
            for item in data {
                let gitUsersData =  Mapper<GitUser>().map(JSON: [:])!
                gitUsersData.followers = Int(item.followers)
                gitUsersData.following = Int(item.following ?? 0)
                gitUsersData.avatar_url = item.avatar_url
                gitUsersData.company = item.company
                gitUsersData.blog = item.blog
                gitUsersData.data = item.imageUser
                gitUsersData.login = item.userName
                dataResult.append(gitUsersData)
            }
            return dataResult

        } catch {
            
        }
        return []
    }
}
