//
//  ListingViewController.swift
//  Abbel-Cars
//
//  Created by Macbook Pro on 02/03/2021.
//  Copyright © 2021 Mian Faizan Nasir. All rights reserved.
//

import UIKit
import ObjectMapper

extension UIImageView {
    func downloaded(from url: URL, contentMode mode: UIView.ContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() { [weak self] in
                self?.image = image
            }
        }.resume()
    }
    func downloaded(from link: String, contentMode mode: UIView.ContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloaded(from: url, contentMode: mode)
    }
}

class ListingViewController: UIViewController, UISearchBarDelegate{

    
    //MARK: - Outlets
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
        
    
    //MARK: -
    var listing = [UserListing]()
    var filterList = [UserListing]()
    var pageNo = 0
    var isSearching = false
    
    //MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        searchBar.delegate = self
        loadData()
        
        tableView.reloadData()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.extendedLayoutIncludesOpaqueBars = false
        self.tableView.contentInset = UIEdgeInsets(top: -86, left: 0, bottom: 0, right: 0)
    }
    
    private func loadData (pageNo: Int = 0) {
        
        UserListing.getUserListing(pageNo: pageNo) { (result, error, status) in
            
            if status == 201 {
                self.listing = CoreDataHandler().getSaveVideoPath()
                return
            }
            
            if error == nil {
                
                for item in result ?? [] {
                    CoreDataHandler().saveVideoPath(login: item.login ?? "", id: Int16(item.id ?? 0), node_id: item.node_id ?? "", avatar_url: item.avatar_url ?? "", gravatar_id: item.gravatar_id ?? "", html_url: item.html_url ?? "", followers_url: item.followers_url ?? "", following_url: item.following_url ?? "", type: item.type ?? "", site_admin: item.site_admin ?? false)
                }
                self.listing.append(contentsOf: result!)
                self.tableView.reloadData()
            }
                        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText != "" {
            
            filterList = listing.filter { (list) -> Bool in
                isSearching = true
                
                if list.login!.contains(searchText.lowercased()) {
                    return true
                }
                return false
            }
            tableView.reloadData()
            return
        }
        isSearching = false
        tableView.reloadData()
        
    }
}


//MARK: - UITableViewDelegate & UITableViewDataSource
extension ListingViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching {
           return filterList.count
        }
        return listing.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.register(ListingTableViewCell.self, indexPath: indexPath)

        
        if !isSearching {
            cell.config(user: listing[indexPath.row],indexPath: indexPath)
            
        } else {
            cell.config(user: filterList[indexPath.row],indexPath: indexPath)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        88
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if scrollView == tableView {
            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height) {
                pageNo += 1
                loadData(pageNo: pageNo)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = GitUserViewController()
        vc.userName = listing[indexPath.row].login ?? ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

