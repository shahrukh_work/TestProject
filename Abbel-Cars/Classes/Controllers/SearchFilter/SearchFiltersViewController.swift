//
//  SearchFiltersViewController.swift
//  Abbel-Cars
//
//  Created by a on 21/08/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit
import ObjectMapper

protocol SearchToCarsDelegate: AnyObject {
    func callFunction ()
 }

class SearchFiltersViewController: UIViewController {
    
    //MARK:- IBOutlets
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var registrationNoTextField: UITextField!
    @IBOutlet weak var carMakeTextField: UITextField!
    @IBOutlet weak var carModelTextField: UITextField!
    @IBOutlet weak var engineCapacityTextField: UITextField!
    
    @IBOutlet weak var selectedColorView: UIView!
    @IBOutlet weak var colorTextField: UITextField!
    @IBOutlet weak var fuelTypeTextField: UITextField!
    @IBOutlet weak var carBadgeTextField: UITextField!
    @IBOutlet weak var lowerKmLabel: UILabel!
    
    @IBOutlet weak var upperKmLabel: UILabel!
    @IBOutlet weak var kmRangeSlider: RangeSlider!
    @IBOutlet weak var transmissionTextField: UITextField!
    @IBOutlet weak var bodyTypeTextField: UITextField!
    @IBOutlet weak var lowerPriceLabel: UILabel!
    
    @IBOutlet weak var upperPriceLabel: UILabel!
    @IBOutlet weak var priceRangeSlider: RangeSlider!
    @IBOutlet weak var makeYearTextField: UITextField!
    @IBOutlet weak var priceButton: UIButton!
    @IBOutlet weak var kmButton: UIButton!
    
    @IBOutlet weak var newlyAddedButton: UIButton!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var applyButton: UIButton!
    
    
    // MARK:- Variables
    var carMake = Mapper<MakeData>().map(JSON: [:])!
    var carModel = Mapper<CarModelData>().map(JSON: [:])!
    var engineCapacity = ["660cc", "800cc", "1,000cc", "1,300cc", "1,500cc", "1,600cc", "1,800cc", "2,000cc", "2,400cc", "2,700cc", "2,800cc", "3,000cc", "4,000cc", "4,500cc"]
    var color = ["White", "Silver", "Gray", "Black", "Red", "Yellow", "Blue", "Brown", "Orange", "Green", "Voilet", "Dandelion", "Apricot", "Scarlet", "Indigo", "Magenta", "Pink"]
    var fuelType = ["Petrol", "Diesel", "CNG", "Electric"]
    var transmission = ["Automatic", "Manual"]
    var bodyType = ["Sedan", "Hatch", "SUV", "Cab Shassis", "Wagon", "Hatchback", "Ute", "Van/Minivan", "Mini Van", "Coupe (2 door)", "Convertible", "Coupe", "Bus", "People Mover", "Commercial", "Truck", "Light Truck", "Fastback", "Utility Dual Cab"]
    var makeYear = ["1990", "1991", "1992", "1993", "1994", "1995", "1996", "1997", "1998", "1999", "2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020"]
    var sortBy = ""
    weak var delegate: SearchToCarsDelegate?
    
    
    //MARK:- Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    
    //MARK:- View Setup
    private func setupView() {
        
        registrationNoTextField.text = UserDefaults.standard.string(forKey: "RegNo") ?? ""
        carMakeTextField.text = UserDefaults.standard.string(forKey: "carMake") ?? ""
        carModelTextField.text = UserDefaults.standard.string(forKey: "carModel") ?? ""
        engineCapacityTextField.text = UserDefaults.standard.string(forKey: "engineCapacity") ?? ""
        colorTextField.text = UserDefaults.standard.string(forKey: "color") ?? nil
        fuelTypeTextField.text = UserDefaults.standard.string(forKey: "fuelType") ?? ""
        carBadgeTextField.text = UserDefaults.standard.string(forKey: "carBadge") ?? ""
        kmRangeSlider.lowerValue = Double(UserDefaults.standard.integer(forKey: "minKM"))
        kmRangeSlider.upperValue = Double(UserDefaults.standard.integer(forKey: "maxKM"))
        transmissionTextField.text = UserDefaults.standard.string(forKey: "transmission") ?? ""
        bodyTypeTextField.text = UserDefaults.standard.string(forKey: "bodyType") ?? ""
        priceRangeSlider.lowerValue = Double(UserDefaults.standard.integer(forKey: "minPrice"))
        priceRangeSlider.upperValue = Double(UserDefaults.standard.integer(forKey: "maxPrice"))
        makeYearTextField.text = String(UserDefaults.standard.integer(forKey: "makeYear"))
        sortBy = UserDefaults.standard.string(forKey: "sortBy") ?? ""
        
        carMakeTextField.inputView = self.pickerView
        carModelTextField.inputView = self.pickerView
        engineCapacityTextField.inputView = self.pickerView
        colorTextField.inputView = self.pickerView
        //colorTextField.text = "Black"
        selectedColorView.backgroundColor = nil
        fuelTypeTextField.inputView = self.pickerView
        
        transmissionTextField.inputView = self.pickerView
        bodyTypeTextField.inputView = self.pickerView
        makeYearTextField.inputView = self.pickerView
        registrationNoTextField.delegate = self
        carMakeTextField.delegate = self
        
        kmRangeSlider.minimumValue = 1
        kmRangeSlider.maximumValue = 100000
        kmRangeSlider.lowerValue = 1
        kmRangeSlider.upperValue = 100000
        
        priceRangeSlider.minimumValue = 1
        priceRangeSlider.maximumValue = 100000
        priceRangeSlider.lowerValue = 1
        priceRangeSlider.upperValue = 100000
        
        carBadgeTextField.delegate = self
        lowerKmLabel.text = String(Int(kmRangeSlider.lowerValue))
        upperKmLabel.text = String(Int(kmRangeSlider.upperValue))
        kmRangeSlider.addTarget(self, action: #selector(self.kmRangeSliderValueChanged(_:)), for: .valueChanged)
        lowerPriceLabel.text = String(Int(priceRangeSlider.lowerValue))
        
        upperPriceLabel.text = String(Int(priceRangeSlider.upperValue))
        priceRangeSlider.addTarget(self, action: #selector(self.priceRangeSliderValueChanged(_:)), for: .valueChanged)
        pickerView.delegate = self
        pickerView.dataSource = self
        applyButton.clipsToBounds = true
        
        applyButton.layer.cornerRadius = 5
        applyButton.applyGradient(colors: [#colorLiteral(red: 0.8705882353, green: 0.2980392157, blue: 0.3725490196, alpha: 1),#colorLiteral(red: 0.862745098, green: 0.1529411765, blue: 0.2509803922, alpha: 1)])
        navigationView.layer.masksToBounds = false
        navigationView.layer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0.1607843137, alpha: 1)
        navigationView.layer.shadowOpacity = 0.05
        
        priceButton.layer.cornerRadius = 21
        priceButton.setTitleColor(#colorLiteral(red: 0.7882352941, green: 0.1568627451, blue: 0.1764705882, alpha: 1), for: .normal)
        priceButton.layer.masksToBounds = false
        priceButton.layer.borderColor = #colorLiteral(red: 0.7882352941, green: 0.1568627451, blue: 0.1764705882, alpha: 1)
        priceButton.layer.borderWidth = 1
        
        kmButton.layer.cornerRadius = 21
        kmButton.setTitleColor(#colorLiteral(red: 0.7882352941, green: 0.1568627451, blue: 0.1764705882, alpha: 1), for: .normal)
        kmButton.layer.masksToBounds = false
        kmButton.layer.borderColor = #colorLiteral(red: 0.7882352941, green: 0.1568627451, blue: 0.1764705882, alpha: 1)
        kmButton.layer.borderWidth = 1
        
        newlyAddedButton.layer.cornerRadius = 21
        newlyAddedButton.setTitleColor(#colorLiteral(red: 0.7882352941, green: 0.1568627451, blue: 0.1764705882, alpha: 1), for: .normal)
        newlyAddedButton.layer.masksToBounds = false
        newlyAddedButton.layer.borderColor = #colorLiteral(red: 0.7882352941, green: 0.1568627451, blue: 0.1764705882, alpha: 1)
        newlyAddedButton.layer.borderWidth = 1
        
        navigationView.layer.shadowOffset = CGSize(width: 0, height: 8)
        navigationView.layer.shadowRadius = 5
        dataSource()
    }
    
    
    //MARK:- DataSource
    func dataSource() {
        getCarMake()
    }
    
    
    //MARK:- IBActions
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func priceButtonTapped(_ sender: Any) {
        self.sortBy = "price"
        priceButton.clipsToBounds = true
        priceButton.layer.cornerRadius = 21
        priceButton.backgroundColor = #colorLiteral(red: 0.8705882353, green: 0.2980392157, blue: 0.3725490196, alpha: 1)
        priceButton.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
        
        kmButton.clipsToBounds = true
        kmButton.layer.cornerRadius = 21
        kmButton.setTitleColor(#colorLiteral(red: 0.7882352941, green: 0.1568627451, blue: 0.1764705882, alpha: 1), for: .normal)
        kmButton.layer.masksToBounds = false
        kmButton.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        kmButton.layer.borderColor = #colorLiteral(red: 0.7882352941, green: 0.1568627451, blue: 0.1764705882, alpha: 1)
        kmButton.layer.borderWidth = 1

        newlyAddedButton.clipsToBounds = true
        newlyAddedButton.layer.cornerRadius = 21
        newlyAddedButton.setTitleColor(#colorLiteral(red: 0.7882352941, green: 0.1568627451, blue: 0.1764705882, alpha: 1), for: .normal)
        newlyAddedButton.layer.masksToBounds = false
        newlyAddedButton.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        newlyAddedButton.layer.borderColor = #colorLiteral(red: 0.7882352941, green: 0.1568627451, blue: 0.1764705882, alpha: 1)
        newlyAddedButton.layer.borderWidth = 1
    }
    
    @IBAction func kmButtonTapped(_ sender: Any) {
        self.sortBy = "km"
        kmButton.clipsToBounds = true
        kmButton.layer.cornerRadius = 21
        kmButton.backgroundColor = #colorLiteral(red: 0.8705882353, green: 0.2980392157, blue: 0.3725490196, alpha: 1)
        kmButton.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
        
        priceButton.clipsToBounds = true
        priceButton.layer.cornerRadius = 21
        priceButton.setTitleColor(#colorLiteral(red: 0.7882352941, green: 0.1568627451, blue: 0.1764705882, alpha: 1), for: .normal)
        priceButton.layer.masksToBounds = false
        priceButton.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        priceButton.layer.borderColor = #colorLiteral(red: 0.7882352941, green: 0.1568627451, blue: 0.1764705882, alpha: 1)
        priceButton.layer.borderWidth = 1

        newlyAddedButton.clipsToBounds = true
        newlyAddedButton.layer.cornerRadius = 21
        newlyAddedButton.setTitleColor(#colorLiteral(red: 0.7882352941, green: 0.1568627451, blue: 0.1764705882, alpha: 1), for: .normal)
        newlyAddedButton.layer.masksToBounds = false
        newlyAddedButton.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        newlyAddedButton.layer.borderColor = #colorLiteral(red: 0.7882352941, green: 0.1568627451, blue: 0.1764705882, alpha: 1)
        newlyAddedButton.layer.borderWidth = 1
    }
    
    @IBAction func newlyAddedButtonTapped(_ sender: Any) {
        self.sortBy = "newly added"
        newlyAddedButton.clipsToBounds = true
        newlyAddedButton.layer.cornerRadius = 21
        newlyAddedButton.backgroundColor = #colorLiteral(red: 0.8705882353, green: 0.2980392157, blue: 0.3725490196, alpha: 1)
        newlyAddedButton.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
        
        priceButton.clipsToBounds = true
        priceButton.layer.cornerRadius = 21
        priceButton.setTitleColor(#colorLiteral(red: 0.7882352941, green: 0.1568627451, blue: 0.1764705882, alpha: 1), for: .normal)
        priceButton.layer.masksToBounds = false
        priceButton.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        priceButton.layer.borderColor = #colorLiteral(red: 0.7882352941, green: 0.1568627451, blue: 0.1764705882, alpha: 1)
        priceButton.layer.borderWidth = 1

        kmButton.clipsToBounds = true
        kmButton.layer.cornerRadius = 21
        kmButton.setTitleColor(#colorLiteral(red: 0.7882352941, green: 0.1568627451, blue: 0.1764705882, alpha: 1), for: .normal)
        kmButton.layer.masksToBounds = false
        kmButton.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        kmButton.layer.borderColor = #colorLiteral(red: 0.7882352941, green: 0.1568627451, blue: 0.1764705882, alpha: 1)
        kmButton.layer.borderWidth = 1
    }
    
    @IBAction func ApplyButtonTapped(_ sender: UIButton) {
        
        DataManager.shared.setStringData(value: registrationNoTextField.text!, key: "RegNo")
        DataManager.shared.setStringData(value: carMakeTextField.text!, key: "carMake")
        DataManager.shared.setStringData(value: carModelTextField.text!, key: "carModel")
        DataManager.shared.setStringData(value: engineCapacityTextField.text!, key: "engineCapacity")
        DataManager.shared.setStringData(value: colorTextField.text!, key: "color")
        DataManager.shared.setStringData(value: fuelTypeTextField.text!, key: "fuelType")
        DataManager.shared.setStringData(value: carBadgeTextField.text!, key: "carBadge")
        DataManager.shared.setIntData(value: Int(kmRangeSlider.lowerValue), key: "minKM")
        DataManager.shared.setIntData(value: Int(kmRangeSlider.upperValue), key: "maxKM")
        DataManager.shared.setStringData(value: transmissionTextField.text!, key: "transmission")
        DataManager.shared.setStringData(value: bodyTypeTextField.text!, key: "bodyType")
        DataManager.shared.setIntData(value: Int(priceRangeSlider.lowerValue), key: "minPrice")
        DataManager.shared.setIntData(value: Int(priceRangeSlider.upperValue), key: "maxPrice")
        DataManager.shared.setIntData(value: Int(makeYearTextField.text == "" ? "0" : makeYearTextField.text!)!, key: "makeYear")
        DataManager.shared.setStringData(value: self.sortBy, key: "sortBy")
        
        let alert = UIAlertController(title: "Success", message: "Filters applied successfully", preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: .default) {
            UIAlertAction in
            self.delegate?.callFunction()
            self.navigationController?.popViewController(animated: true)
        }
         alert.addAction(okAction)
         self.present(alert, animated: true, completion: nil)
    }
    
    
    //MARK:- Methods
    func getCarMake() {
        Makes.getCarMakes { [weak self](response, error, status) in
            if error == nil {
                self?.carMake = response!
            }
        }
    }
    
    func getCarModels(id: Int) {
        CarModels.getCarModels(id: id) { [weak self](response, error, status) in
            if error == nil {
                self?.carModel = response!
            }
        }
    }
    
    
    //MARK:- Selectors
    @objc func kmRangeSliderValueChanged(_ rangeSlider: RangeSlider) {
        lowerKmLabel.text = String(Int(rangeSlider.lowerValue))
        upperKmLabel.text = String(Int(rangeSlider.upperValue))
    }
    
    @objc func priceRangeSliderValueChanged(_ rangeSlider: RangeSlider) {
        lowerPriceLabel.text = "$ \(Int(rangeSlider.lowerValue))"
        upperPriceLabel.text = "$ \(Int(rangeSlider.upperValue))"
    }
}


//MARK:- Textfield Delegates & DataSource
extension SearchFiltersViewController : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 50
        let currentString: NSString = (textField.text ?? "") as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
}


//MARK:- Pickerview Delegates
extension SearchFiltersViewController : UIPickerViewDataSource, UIPickerViewDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if engineCapacityTextField.isFirstResponder {
            return engineCapacity.count
            
        } else if carMakeTextField.isFirstResponder {
            return carMake.make.count
            
        } else if carModelTextField.isFirstResponder {
            if carModel.count > 0 {
                return carModel.data.count
            }
            return 0
            
        } else if colorTextField.isFirstResponder {
            return color.count
            
        } else if fuelTypeTextField.isFirstResponder {
            return fuelType.count
            
        } else if transmissionTextField.isFirstResponder {
            return transmission.count
            
        } else if bodyTypeTextField.isFirstResponder {
            return bodyType.count
            
        } else if makeYearTextField.isFirstResponder {
            return makeYear.count
            
        } else  {
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if engineCapacityTextField.isFirstResponder {
            return engineCapacity[row]
            
        } else if carMakeTextField.isFirstResponder {
            return carMake.make[row].name
            
        } else if carModelTextField.isFirstResponder {
            if carModel.count > 0 {
                return carModel.data[row].name
            }
            return ""
            
        } else if colorTextField.isFirstResponder {
            return color[row]
            
        } else if fuelTypeTextField.isFirstResponder {
            return fuelType[row]
            
        } else if transmissionTextField.isFirstResponder {
            return transmission[row]
            
        } else if bodyTypeTextField.isFirstResponder {
            return bodyType[row]
            
        } else if makeYearTextField.isFirstResponder {
            return makeYear[row]
        }
        
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if engineCapacityTextField.isFirstResponder {
            let itemSelected = engineCapacity[row]
            engineCapacityTextField.text = itemSelected
            
        } else if carMakeTextField.isFirstResponder {
            let itemSelected = carMake.make[row].name
            carMakeTextField.text = itemSelected
            carModelTextField.text = nil
            getCarModels(id: carMake.make[row].id)
            
        } else if carModelTextField.isFirstResponder {
            let itemSelected = carModel.data[row].name
            carModelTextField.text = itemSelected
            
        } else if colorTextField.isFirstResponder {
            let itemSelected = color[row]
            colorTextField.text = itemSelected
            
            switch(colorTextField.text){
                
                case "Red":
                    selectedColorView.backgroundColor = #colorLiteral(red: 1, green: 0.2705882353, blue: 0.2274509804, alpha: 1)
                
                case "Brown":
                    selectedColorView.backgroundColor = #colorLiteral(red: 0.6679978967, green: 0.4751212597, blue: 0.2586010993, alpha: 1)
                    
                case "Orange":
                    selectedColorView.backgroundColor = #colorLiteral(red: 1, green: 0.5763723254, blue: 0, alpha: 1)
                
                case "Voilet":
                    selectedColorView.backgroundColor = #colorLiteral(red: 0.9333333333, green: 0.5098039216, blue: 0.9333333333, alpha: 1)
                
                case "Silver":
                    selectedColorView.backgroundColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
                
                case "Indigo":
                    selectedColorView.backgroundColor = #colorLiteral(red: 0.368627451, green: 0.3607843137, blue: 0.9019607843, alpha: 1)
                
                case "Magenta":
                    selectedColorView.backgroundColor = #colorLiteral(red: 1, green: 0.2527923882, blue: 1, alpha: 1)
                
                case "Dandelion":
                    selectedColorView.backgroundColor = #colorLiteral(red: 0.9411764706, green: 0.8823529412, blue: 0.1882352941, alpha: 1)
                
                case "Apricot":
                    selectedColorView.backgroundColor = #colorLiteral(red: 0.9843137255, green: 0.8078431373, blue: 0.6941176471, alpha: 1)
                
                case "Scarlet":
                    selectedColorView.backgroundColor = #colorLiteral(red: 1, green: 0.1411764706, blue: 0, alpha: 1)
                
                case "Black":
                    selectedColorView.backgroundColor = #colorLiteral(red: 0.06666666667, green: 0.06666666667, blue: 0.06666666667, alpha: 1)
                
                case "Blue":
                    selectedColorView.backgroundColor = #colorLiteral(red: 0.01680417731, green: 0.1983509958, blue: 1, alpha: 1)
                
                case "Yellow":
                    selectedColorView.backgroundColor = #colorLiteral(red: 0.9994240403, green: 0.9855536819, blue: 0, alpha: 1)
                
                case "Pink":
                    selectedColorView.backgroundColor = #colorLiteral(red: 0.8549019694, green: 0.250980407, blue: 0.4784313738, alpha: 1)
                
                case "Gray":
                    selectedColorView.backgroundColor = #colorLiteral(red: 0.5568627451, green: 0.5568627451, blue: 0.5764705882, alpha: 1)
                
                case "White":
                    selectedColorView.backgroundColor = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1)
                
                case "Green":
                    selectedColorView.backgroundColor = #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)
                
                default:
                    break
            }
            
        } else if fuelTypeTextField.isFirstResponder {
            let itemSelected = fuelType[row]
            fuelTypeTextField.text = itemSelected
            
        } else if transmissionTextField.isFirstResponder {
            let itemSelected = transmission[row]
            transmissionTextField.text = itemSelected
            
        } else if bodyTypeTextField.isFirstResponder {
            let itemSelected = bodyType[row]
            bodyTypeTextField.text = itemSelected
            
        } else if makeYearTextField.isFirstResponder {
            let itemSelected = makeYear[row]
            makeYearTextField.text = itemSelected
        }
        //self.view.endEditing(true)
    }
}
