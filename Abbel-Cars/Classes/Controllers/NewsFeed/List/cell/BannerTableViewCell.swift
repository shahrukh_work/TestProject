//
//  BannerTableViewCell.swift
//  Abbel-Cars
//
//  Created by Mac on 15/09/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit
import SDWebImage

class BannerTableViewCell: UITableViewCell {

    
    //MARK: - Outlet
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var bannerImageView: UIImageView!


    //MARK: - Variable


    //MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        selectionStyle = .none
    }
    
    func configure () {
        
    }
}
