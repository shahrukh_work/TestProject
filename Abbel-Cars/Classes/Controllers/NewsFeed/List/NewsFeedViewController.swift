//
//  NewsFeedViewController.swift
//  Abbel-Cars
//
//  Created by Mac on 15/09/2020.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit
import ObjectMapper
import SDWebImage

class NewsFeedViewController: UIViewController {

    
    //MARK: - Outlet
    @IBOutlet weak var tableView: UITableView!
    

    //MARK: - Variable
    var tableData = Mapper<NewFeedData>().map(JSON: [:])!


    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        dataSource()
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    
    //MARK: - Setup
    func setupView () {
        self.navigationController?.navigationBar.isHidden = true
        tableView.delegate = self
        tableView.dataSource = self
        dataSource()
    }
    
    
    //MARK: - DataSource
    func dataSource () {
        getNewsFeed()
    }
    
    //MARK: - Actions
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK: - Methods
    func getNewsFeed () {
        NewsFeed.getNewsFeed(limit: 1000) { [weak self] (result, error, status) in
            if error == nil {
                self?.tableData = result!
                self?.tableView.reloadData()
            }
        }
    }
}


//MARK: - UITableViewDelegate & UITableViewDataSource
extension NewsFeedViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        tableData.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.register(BannerTableViewCell.self, indexPath: indexPath)
        
        if let url = URL(string: APIRoutes.baseUrl + APIRoutes.feedImageBase + (self.tableData.data[indexPath.row].image)) {
            cell.bannerImageView.sd_setImage(with: url, placeholderImage: nil)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let promoVC = PromotionDetailViewController(nibName: "PromotionDetailViewController", bundle: nil)
        promoVC.titleText = tableData.data[indexPath.row].title
        promoVC.image = tableData.data[indexPath.row].image
        promoVC.message = tableData.data[indexPath.row].message
        self.navigationController?.pushViewController(promoVC, animated: true)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
